"""

Murano service controller class.

*** Responsibility:
    - Activation
    - Read data from the service
    - Upload sensor data to the service

"""
import datetime
import logging.config
import os
import platform
import socket
import ssl
import threading
import time
import traceback

try:
    from StringIO import StringIO
    import httplib
    input = raw_input
    PYTHON = 2
except ImportError:
    from http import client as httplib
    from io import StringIO, BytesIO
    PYTHON = 3

# -----------------------------------------------------------------
# ---- SHOULD NOT NEED TO CHANGE ANYTHING BELOW THIS LINE ------
# -----------------------------------------------------------------
host_address_base = os.getenv('EXOSITE_HOST', 'm2.exosite.com')
https_port = 443

cur_dir = os.path.dirname(os.path.realpath(__file__)) + '/'

try:
    logging.config.fileConfig(cur_dir + "logging.conf")
except IOError:
    print 'Failed to load configuration of logging'

logger = logging.getLogger("murano")


class FakeSocket:
    def __init__(self, response_str):
        if PYTHON == 2:
            self._file = StringIO(response_str)
        else:
            self._file = BytesIO(response_str)

    def makefile(self, *args, **kwargs):
        return self._file


class MuranoCtrl:
    """
    Usage:
        After declaring instance of this class, please activate by calling `instance.activate()` function.
        And then we can use upload_to_murano(), read_from_murano(), long_poll_wait() functions successfully.

    """
    last_modified = {}
    product_id = ''
    identifier = ''
    long_poll_timeout = 2000
    host_address = None
    cik = None
    lock = threading.RLock()

    def __init__(self, product_id='', identifier='', device_cik='', long_poll_timeout=2000):
        # We assume that Gateway table is already configured in setup.py
        self.product_id = product_id
        self.identifier = identifier
        self.cik = device_cik
        self.long_poll_timeout = long_poll_timeout
        self.host_address = self.product_id + '.' + host_address_base
        logger.debug('Starting Murano Controller, PID: {}, ID: {}, CIK: {}'.format(product_id, identifier, device_cik))
        if platform.system() == 'Linux':
            time.sleep(5)

    def send_socket(self, http_packet):
        """
        # SEND REQUEST
        :param http_packet:
        :return: Parsed response
        """
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        ssl_s = ssl.wrap_socket(s)
        try:
            ssl_s.connect((self.host_address, https_port))
        except socket.error as e:
            logger.exception('Failed to send socket: {}'.format(e))
            return None
        except Exception as e:
            logger.exception('Failed to send socket: {}'.format(e))
            return None

        if PYTHON == 2:
            ssl_s.send(http_packet)
        else:
            ssl_s.send(bytes(http_packet, 'UTF-8'))
        # GET RESPONSE
        response = ssl_s.recv(1024)
        ssl_s.close()

        # PARSE RESPONSE
        fake_socket_response = FakeSocket(response)
        parsed_response = httplib.HTTPResponse(fake_socket_response)
        parsed_response.begin()
        return parsed_response

    def activate(self):
        """
        Activate product with CIK data in the local file
        Send activate packet to the server with given product id and identifier.
        :return: When success, returns new CIK data, otherwise returns None or False
        """
        try:
            logger.debug("attempt to activate on Murano")
            http_body = 'vendor=' + self.product_id + '&model=' + self.product_id + '&sn=' + self.identifier
            # BUILD HTTP PACKET
            http_packet = ""
            http_packet += 'POST /provision/activate HTTP/1.1\r\n'
            http_packet += 'Host: ' + self.host_address + '\r\n'
            http_packet += 'Connection: Close \r\n'
            http_packet += 'Content-Type: application/x-www-form-urlencoded; charset=utf-8\r\n'
            http_packet += 'content-length:' + str(len(http_body)) + '\r\n'
            http_packet += '\r\n'
            http_packet += http_body

            response = self.send_socket(http_packet)
            if response is None:
                return None, 'Connection error'

            # HANDLE POSSIBLE RESPONSES
            if response.status == 200:
                new_cik = response.read().decode("utf-8")
                logger.debug("Activation Response: New CIK: {} ..............................".format(new_cik[0:10]))
                # Save new cik value to the local variable... IMPORTANT
                self.cik = new_cik
                return True, new_cik
            elif response.status == 409:
                msg = "Activation Response: Device was Already Activated, there is no new CIK"
                logger.error(msg)
                return False, msg

            elif response.status == 404:
                msg = "Activation Response: Device Identity ({}) is not available or check Product Id ({})".format(
                    self.identifier,
                    self.product_id
                    )
                logger.error(msg)
                return None, msg
            else:
                msg = "Activation Response: failed request: {} {}".format(str(response.status), response.reason)
                logger.error(msg)
                return None, msg
        except:
            logger.exception('Unknown exception: {}'.format(traceback.print_exc()))
            return None, traceback.format_exc()

    def upload_to_murano(self, write_params):
        """
        Upload data to Murano
        :param write_params:
        :return:
        """
        with self.lock:
            try:
                http_body = write_params
                # BUILD HTTP PACKET
                http_packet = ""
                http_packet += 'POST /onep:v1/stack/alias HTTP/1.1\r\n'
                http_packet += 'Host: ' + self.host_address + '\r\n'
                http_packet += 'X-EXOSITE-CIK: ' + self.cik + '\r\n'
                http_packet += 'Connection: Close \r\n'
                http_packet += 'Content-Type: application/x-www-form-urlencoded; charset=utf-8\r\n'
                http_packet += 'content-length:' + str(len(http_body)) + '\r\n'
                http_packet += '\r\n'
                http_packet += http_body

                response = self.send_socket(http_packet)
                if response is None:
                    return None

                # HANDLE POSSIBLE RESPONSES
                if response.status == 204:
                    # print "write success"
                    return True, 204
                elif response.status == 401:
                    logger.error("Failed to upload data, 401: Bad Auth, CIK may be bad")
                    return False, 401
                elif response.status == 400:
                    logger.error("Failed to upload data, 400: Bad Request: check syntax")
                    return False, 400
                elif response.status == 405:
                    logger.error("Failed to upload data, 405: Bad Method")
                    return False, 405
                else:
                    logger.error("Failed to upload, unknown Error: {} {}".format(response.status, response.reason))
                    return False, response.status
            except:
                traceback.print_exc()
                return False, -1

    def read_from_murano(self, read_params):
        """
        Read data from Murano service
        :param read_params:
        :return:
        """
        with self.lock:
            try:
                # BUILD HTTP PACKET
                http_packet = ""
                http_packet += 'GET /onep:v1/stack/alias?' + read_params + ' HTTP/1.1\r\n'
                http_packet += 'Host: ' + self.host_address + '\r\n'
                http_packet += 'X-EXOSITE-CIK: ' + self.cik + '\r\n'
                # http_packet += 'Connection: Close \r\n'
                http_packet += 'Accept: application/x-www-form-urlencoded; charset=utf-8\r\n'
                http_packet += '\r\n'

                response = self.send_socket(http_packet)

                # HANDLE POSSIBLE RESPONSES
                if response.status == 200:
                    # print "read success"
                    return True, response.read().decode('utf-8')
                elif response.status == 204:        # Successful request, nothing will be returned
                    return True, ''
                elif response.status == 401:
                    logger.error("Failed to read, 401: Bad Auth, CIK may be bad")
                    return False, 401
                elif response.status == 400:
                    logger.error("Failed to read, 400: Bad Request: check syntax")
                    return False, 400
                elif response.status == 405:
                    logger.error("Failed to read, 405: Bad Method")
                    return False, 405
                else:
                    logger.error("Failed to read, unknown error: {} {}".format(response.status, response.reason))
                    return False, response.status

            except:
                traceback.print_exc()
                return False, 'function exception'

    def long_poll_wait(self, read_params):
        """
        Long poll state wait request from Murano
        :param read_params:
        :return:
        """
        with self.lock:
            try:
                # BUILD HTTP PACKET
                http_packet = ""
                http_packet += 'GET /onep:v1/stack/alias?' + read_params + ' HTTP/1.1\r\n'
                http_packet += 'Host: ' + self.host_address + '\r\n'
                http_packet += 'Accept: application/x-www-form-urlencoded; charset=utf-8\r\n'
                http_packet += 'X-EXOSITE-CIK: ' + self.cik + '\r\n'
                http_packet += 'Request-Timeout: ' + str(self.long_poll_timeout) + '\r\n'
                if self.last_modified.get(read_params) is not None:
                    http_packet += 'If-Modified-Since: ' + self.last_modified.get(read_params) + '\r\n'
                http_packet += '\r\n'

                response = self.send_socket(http_packet)
                if response is None:
                    return None

                # HANDLE POSSIBLE RESPONSES
                if response.status == 200:
                    # print "read success"
                    if response.getheader("last-modified") is not None:
                        # Save Last-Modified Header (Plus 1s)
                        lm = response.getheader("last-modified")
                        next_lm = (datetime.datetime.strptime(lm, "%a, %d %b %Y %H:%M:%S GMT") +
                                   datetime.timedelta(seconds=1)).strftime("%a, %d %b %Y %H:%M:%S GMT")
                        self.last_modified[read_params] = next_lm
                    return True, response.read()
                elif response.status == 304:
                    # print "304: No Change"
                    return False, 304
                elif response.status == 401:
                    print("401: Bad Auth, CIK may be bad")
                    return False, 401
                elif response.status == 400:
                    print("400: Bad Request: check syntax")
                    return False, 400
                elif response.status == 405:
                    print("405: Bad Method")
                    return False, 405
                else:
                    print(str(response.status), response.reason)
                    return False, response.status

            except:
                traceback.print_exc()
                return False, 'function exception'

# -----------------------------------------------------------------
# --------  SAMPLE SCRIPT FOR lightbulb example product  ----------
# -----------------------------------------------------------------

# Sensor data
state = ''
temperature = 70
humidity = 50
uptime = 0
connected = 'Disconnected'


if __name__ == '__main__':

    inst = MuranoCtrl()
    # print inst.upload_to_portal('773bb71fcdd1a41237d0f20993b0e09363ecc3f1', '4.0')

    inst = MuranoCtrl()

    print inst.activate()
    # start_time = int(time.time())
    # print("\r\n-----")
    # print("Murano Device Simulator booting...")
    # print("Product Id: {}".format(inst.product_id))
    # print("Device Identity: {}".format(inst.identifier))
    # print("Product Unique Host: {}".format(inst.host_address))
    # print("-----")
    #
    # if not inst.activate():
    #     print "Failed to activation, please check Product ID and Identifier value"
    #     sys.exit(1)
    #
    # # --------------------------
    # # MAIN LOOP
    # # --------------------------
    # print("starting main loop")
    #
    # counter = 100  # for debug purposes so you don't have issues killing this process
    # LOOP = True
    # lightbulb_state = 0
    # init = 1
    #
    # while LOOP:
    #     uptime = int(time.time()) - start_time
    #     last_request = time.time()
    #
    #     output_string = (
    #         "Connection: {0:s}, Run Time: {1:5d}, Temperature: {2:3.1f} F, Humidity: {3:3.1f} %, "
    #         "Light State: {4:1d}").format(connected, uptime, temperature, humidity, lightbulb_state)
    #     print("{}".format(output_string))
    #
    #     temperature = round(random.uniform(temperature - 0.2, temperature + 0.2), 1)
    #     if temperature > 120:
    #         temperature = 120
    #     if temperature < 1:
    #         temperature = 1
    #     # GENERATE RANDOM HUMIDITY VALUE
    #     humidity = round(random.uniform(humidity - 0.2, humidity + 0.2), 1)
    #     if humidity > 100:
    #         humidity = 100
    #     if humidity < 1:
    #         humidity = 1
    #
    #     status, resp = inst.upload_to_murano('temperature=' + str(temperature) + '&humidity=' +
    #                                          str(humidity) + '&uptime=' + str(uptime))
    #
    #     connected = 'Connected' if status else 'Disconnected'
    #
    #     status, resp = inst.long_poll_wait('state')
    #     if status:
    #         # print("New State Value: {}".format(str(resp)))
    #         new_value = resp.split('=')
    #         if lightbulb_state != int(new_value[1]):
    #             lightbulb_state = int(new_value[1])
    #             if lightbulb_state == 1:
    #                 print("Action -> Turn Light Bulb On")
    #             else:
    #                 print("Action -> Turn Light Bulb Off")
    #
    #     if not status and resp == 401:
    #         print "Activation is required..."
    #         connected = 'Connected' if inst.activate() else 'Disconnected'