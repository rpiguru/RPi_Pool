# RPi-based product to measure pH/Temp/Chlorine for private swimming pools.

This product will have the same functionality as Aseko REDOX.

http://orders.aseko.com/en/dosing-and-control-system/375-asin-aqua-redox

## Components
    
- Raspberry Pi 3.
    
    https://www.adafruit.com/products/3055
    
- Adafruit PiTFT 3.5" touch screen
    
    https://www.adafruit.com/product/2441
    
- ADS1115 16-Bit ADC - 4 Channel with Programmable Gain Amplifier
    
    https://www.adafruit.com/product/1085

- Temperature Sensor
    
    https://store.brewpi.com/temperature-control/temperature-sensors/threaded-onewire-temperature-sensor
    
- PH sensor
    
    http://dk.hach.com/ph-electrode-5300-plast-gel-electrolyte-0-80-c-6-bar/product-downloads?id=25141350657&callback=qs
    
    We need additional amplifier module for pH sensors:
    
    http://www.banggood.com/Liquid-PH-Value-Detection-Monitoring-Control-Detect-Sensor-Module-For-Arduino-p-1091004.html

- ORP sensor
    
    https://www.amazon.com/Sensorex-S465C-Replacement-Measurement-Applications/dp/B00HU249V2/ref=pd_sim_328_1?ie=UTF8&dpID=411C%2BQjZ%2B%2BL&dpSrc=sims&preST=_AC_UL160_SR160%2C160_&refRID=17G37X1Y5YARG584ZXKE
    
    Or: 
    
    http://dk.hach.com/redox-electrode-5361-gel-ceramic-diaphragm-80-c-6-bar/product-downloads?id=25141350672&callback=qs

    We need BNC term for this sensor:
    
    http://nl.mouser.com/ProductDetail/Gravitech/BNC-TERM/?qs=sGAEpiMZZMv9Q1JI0Mo%2ftV4CfLYKXjYT
    
- Two pneumatic pumps 
    
    Welco peristaltic pump WP10-P1/8M2-J4-C
    
    Detail:     (http://www.welco.net/product/wp1000_1100/wp1000_1100_guide.html)
    
        WP10: WP1000
        P1/8: 1/8" PHARMED BPT
        M  : DC24V Brush Motor 150RPM, 300mA
        2  : 2 Rollers
        J4 : Nut and sleeve are integrated. Excellent workability, 1/4”(6.4mm) (Nylon or Polyethylene)
    
    Since its working voltage is 24V, we need additional driver board.
    
    https://www.adafruit.com/product/3190

- Flow Sensor
    
    www.ebay.com.au/itm/131304513571
    
    
## Wiring Components
  
**NOTE:** You have to turn RPi off before connecting any component!

    
### Wiring Adafruit's 3.5" TFT touch screen.
    
Adafruit provides very detailed tutorial for assembling their TFT touch screen with RPi.

https://learn.adafruit.com/adafruit-pitft-3-dot-5-touch-screen-for-raspberry-pi
    

### Raspberry Pi & BrewPi Temperature Sensor.

- Connecting Temperature sensor

    BrewPi Temperature sensor has RJ11 output and we do not use RJ11 adapter.
    Just cut it and you can see 3 wires colored:
    
        Red = 5V 
        Black = GND 
        White = OneWire data
    
    Now Let's connect it to RPi.
    
    | **RPi**  |  **Temp Sensor**  |
    | :-----:  |  :-----:          |
    |    5V    |  5V (Red)         |
    |  GND     |    GND (Black)    |
    | GPIO4    |    Data (White)   |
    
    
### Raspberry Pi and pH/ORP sensor
   
   Since PH/ORP sensors have analog output, we need ADC converter to read these values.
   
   Adafruit's ADS1115 breakout board supports 4 ADC channels and I2C protocol.
   
   To connect ADS1115 with RPi, please follow the link below:
    
   https://learn.adafruit.com/raspberry-pi-analog-to-digital-converters/ads1015-slash-ads1115
   
   After connecting ADS1115 with RPi, please connect pH/ORP sensor to ADS1115's AIN0/AIN1.
    
   | **RPi**  |  **Flow**       |
   | :-----:  |  :-----:        |
   |  GND     |    GND(Black)   |
   | VCC      |    VDD(Red)     |
   | AIN1(ADS)|    DOUT(Yellow) |
    
   | **RPi**  |  **pH**       |
   | :-----:  |  :-----:        |
   |  GND     |    GND(Black)   |
   | VCC(5V)  |    VDD(Red)     |
   | AIN0(ADS)|    PO(Yellow)   |
    
### Raspberry Pi and Flow Sensor
    
| **RPi**  |  **Flow Sensor**  |
| :-----:  |  :-----:       |
|  GND     |    GND(Black)  |
| GPIO22   |    DOUT(Yellow)|
| 3.3V     |    VCC(Red)    |      
    
    
### Raspberry Pi and Pump.
    
Adafruit provides very detailed document for wiring DRV8871 breakout.

https://learn.adafruit.com/adafruit-drv8871-brushed-dc-motor-driver-breakout/assembly?view=all

**NOTE:** We do not need to connect VCC pin of RPi!

- RPi & DRV8871-1

    | **RPi**  |  **DRV8871-1**  |
    | :-----:  |  :-----:      |
    |  GND     |    IN1        |
    | GPIO12   |    IN2        |

- RPi & DRV8871-2

    | **RPi**  |  **DRV8871-2**  |
    | :-----:  |  :-----:      |
    |  GND     |    IN1        |
    | GPIO13   |    IN2        |
 

## Preparing Raspberry Pi.

- Install `Raspbian` and driver for our touch screen.
    
    Download image from http://director.downloads.raspberrypi.org/raspbian/images/raspbian-2016-11-29/2016-11-25-raspbian-jessie.zip and install.
    (*Note:* Other images will not work, please just install this image.)
    
    To install downloaded `.img` file on your Micro SD card, please follow this:

        http://trendblog.net/install-raspbian-sd-card-os-x-windows/

    Follow the tutorial from https://learn.adafruit.com/running-opengl-based-games-and-emulators-on-adafruit-pitft-displays/pitft-setup
        
        sudo apt-get update
        curl -O https://raw.githubusercontent.com/adafruit/Raspberry-Pi-Installer-Scripts/master/pitft-fbcp.sh
        sudo bash pitft-fbcp.sh
        
    **Important Note:**
    
    When executing `sudo bash pitft-fbcp.sh`, please select `4. Configure options manually` -> `4. PiTFT / PiTFT Plus 3.5"` ->
    `1. Normal (landscape)` -> `4. 270`
     
    After rebooting, you will noticed that touch screen does not work correctly, but no problem.
    
    We just need to run our Kivy GUI app correctly!
    
After installing `raspbian` on your RPi and wiring all components, we need to install some packages.

- Install *Kivy* for Touch Screen GUI app (Leave this for me).
    
        sudo apt-get install -y libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev libsdl2-ttf-dev pkg-config libgl1-mesa-dev libgles2-mesa-dev python-setuptools libgstreamer1.0-dev git-core
        sudo apt-get install -y gstreamer1.0-plugins-{bad,base,good,ugly} gstreamer1.0-{omx,alsa} python-dev
        
        sudo pip install cython==0.23
    
    Install Kivy on RPi
        
        sudo pip install git+https://github.com/kivy/kivy.git@master
        
    Let's configure some settings.
    
        sudo nano /root/.kivy/config.ini
    
    And change like below:
        
        height = 320
        width = 480
        keyboard_mode = dock
        
        show_cursor = 0
        
        [input]
        mouse = mouse
        mtdev_%(name)s = probesysfs,provider=mtdev,param=rotation=270,param=invert_y=1
        hid_%(name)s = probesysfs,provider=hidinput,param=rotation=270,param=invert_y=1

- Remove mouse cursor
    
        sudo nano /etc/lightdm/lightdm.conf
    
    Change `#xserver_command = X` to `xserver_command = X -nocursor`
    
    And reboot.

- Install dependencies
        
        sudo easy_install --upgrade pip
        
        sudo pip install wifi
        sudo chmod 777 /etc/network/interfaces
        sudo pip install wiringpi
        
- Prevent Screen Saver
    
    Add follow in `/etc/rc.local` before `exit 0`:
        
        sudo sh -c "TERM=linux setterm -blank 0 >/dev/tty0"
    
    And do some other stuff:
    
        sudo apt-get install x11-xserver-utils
        sudo nano /etc/X11/xinit/xinitrc
        
    Add following at the end of the file.

        xset s off         # don't activate screensaver
        xset -dpms         # disable DPMS (Energy Star) features.
        xset s noblank     # don't blank the video device
    Change another file.
    
        sudo nano /etc/lightdm/lightdm.conf
        
    In the *SeatDefaults* section it gives the command for starting the X server which
    
    I modified to get it to turn off the screen saver as well as dpms.
    
        [SeatDefaults]
        xserver-command=X -s 0 -dpms

- Testing temperature sensor data.

    Before you can use any 1-wire devices you must first tell the Raspberry Pi how to read them. Open a Terminal Window and type the following to edit the Raspberry Pi’s configuration file:
    
        sudo nano /boot/config.txt
        
    Look to see whether there is a line that has `dtoverlay=w1-gpio` in it.  If not, add the following to the end of the file:
    
        dtoverlay=w1-gpio
        
    Now reboot the Pi:
    
        sudo reboot
        
    To test the configuration, set-up the circuit above to connect the Temp sensor and type the following into a terminal window:
    
        sudo modprobe w1-gpio
        sudo modprobe w1-therm
        cd /sys/bus/w1/devices
        ls
        
    This will list all the devices that are connected to the 1-wire interface.  
    The OneWire Temperature sensor would start with `28-` followed by a long number.  
    Type in the following, replacing the ‘xxxx’ with the text following the `28-`:
    
        cd 28-xxxx
        cat w1_slave
        
    In response, you should get the following showing that the temperature sensor is working:
    
        a3 01 4b 46 7f ff 0e 10 d8 : crc=d8 YES
        a3 01 4b 46 7f ff 0e 10 d8 t=32768
        
- Download latest source code
    
        cd ~
        git clone https://rpi_guru@bitbucket.org/rpi_guru/rpi_pool.git

- Switch default GUI login user from `pi` to `root`
    
        sudo nano /etc/lightdm/lightdm.conf
        
    Change the default user from
        
        autologin-user=pi
    
    to:
    
        autologin-user=root
    
    And reboot.

- Enable auto-start app

    At first we have to start fbcp before executing our app
    
        sudo nano /etc/rc.local
    
    And add this before `exit 0`

        (sleep 5; python /home/pi/rpi_pool/main.py)&
    
- Test GUI app manually
    
        sudo python /home/pi/rpi_pool/main.py 