"""
User Name: pi
PWD: FJtu20050101Ridd

**** pH Calibration.

    pH(correct) = pH(captured) + ZP + (T - 25) * 0.05916 * S / 100

    ZP(Zero point) : pH value at which the total output electrode voltage is equal to 0 mV.
    S(Slope): the amount of changes when temperature is changed 1 degree from the 25'C.
         The typical amount is 0.05916 and this Slope is expressed as percentage of this value.
         e.g : if changed amount is 0.06, Slope is 101(%)

    T : Temperature of the liquid.

    Calibration Formula

    Let's say that we are performing 2-point calibration with pH7, pH4 buffers

    pH_7 = 7.0 + (T_7 - 25) * 0.05916 * S / 100 + ZP
    pH_4 = 4.0 + (T_4 - 25) * 0.05916 * S / 100 + ZP

        where   pH_7 : detected value when probe is placed in pH7 buffer
                pH_4 : detected value when probe is placed in pH4 buffer
                T_7  : Temperature of pH7 buffer
                T_4  : Temperature of pH4 buffer

    S = (pH_7 - pH_4 - 3) / (T_7 - T_4) / 0.05916 * 100
    ZP = pH_7 - 7 - (T_7 - 25) * 0.05916 * S / 100

**** Working with ORP sensor.



"""
import logging
import os
import random
import time

import temp_reader
from base import Base

try:
    is_rpi = True if os.uname()[4][:3] == 'arm' else False
except AttributeError:
    is_rpi = False

if is_rpi:
    try:
        import Adafruit_ADS1x15
    except ImportError:
        print 'Failed to import Adafruit_ADS1x15'


class RPiPool(Base):

    log_level = 10
    log_file_name = ''

    ads_gain = 2/3
    rate = 60
    i2c_address = 0x48
    ph_ads_pin_num = 0
    orp_ads_pin_num = 1
    o2_ads_pin_num = 2
    adc = None
    sample_cnt = 10
    zp = 0.0
    slope = 100

    def __init__(self):
        Base.__init__(self)

    def update_params(self):
        """
        update parameters from configuration file
        :return:
        """
        self.ads_gain = int(self.get_param_from_xml('ADS_GAIN'))
        self.ph_ads_pin_num = int(self.get_param_from_xml('pH_ADS_PIN_NUM'))
        self.orp_ads_pin_num = int(self.get_param_from_xml('ORP_ADS_PIN_NUM'))

        self.sample_cnt = int(self.get_param_from_xml('SAMPLE_COUNT'))
        self.zp = float(self.get_param_from_xml('pH_ZP'))
        self.slope = float(self.get_param_from_xml('pH_SLOPE'))

        self.i2c_address = int(self.get_param_from_xml('I2C_ADDRESS'), 16)
        try:
            self.adc = Adafruit_ADS1x15.ADS1115(address=self.i2c_address)
        except IOError:
            print 'Failed to detect ADS1115 module. Please check!'
            return False
        except NameError:
            return None

    def get_ph_val(self):
        if is_rpi and not self.debug:
            val_list = []
            for i in range(self.sample_cnt):
                val = self.adc.read_adc(self.ph_ads_pin_num, gain=self.ads_gain)
                voltage = val * 4.096 / self.ads_gain / 65536.0 * 2         # multiple 2 due to the signed/unsigned
                ph_val = float(self.get_param_from_xml('LAST_BUF')) + (voltage - float(
                    self.get_param_from_xml('LAST_PH_VOLT'))) / float(self.get_param_from_xml('pH_GRADIENT'))
                val_list.append(ph_val)
                time.sleep(0.05)
            # remove max/min value from the list
            val_list.remove(max(val_list))
            val_list.remove(min(val_list))
            # get average value in list
            avg_val = reduce(lambda x, y: x + y, val_list) / len(val_list)
            return round(avg_val, 1)
        else:
            return random.randint(60, 90) / 10.0

    def get_orp_val(self):
        if is_rpi and not self.debug:
            val_list = []
            for i in range(self.sample_cnt):
                val = self.adc.read_adc(self.orp_ads_pin_num, gain=self.ads_gain)
                orp_val = val * 4.096 / self.ads_gain / 65536.0 * 2         # multiple 2 due to the signed/unsigned
                val_list.append(orp_val)
                time.sleep(0.05)
            # remove max/min value from the list
            val_list.remove(max(val_list))
            val_list.remove(min(val_list))
            # get average value in list
            avg_val = reduce(lambda x, y: x + y, val_list) / len(val_list)
            # Add offset to calibrate after converting V to mV
            return int(avg_val * 1000 + float(self.get_param_from_xml('ORP_OFFSET')))
        else:
            return random.randint(200, 500)

    def get_temperature(self):
        if is_rpi and not self.debug:
            val_list = []
            for i in range(self.sample_cnt):
                s_time = time.time()
                while True:
                    temp_c, temp_f = temp_reader.read_temp()
                    if temp_c is not None:
                        val_list.append(temp_c)
                        break
                    else:
                        if time.time() - s_time > 5:
                            return None, temp_f

                time.sleep(0.1)
            # remove max/min value from the list
            val_list.sort()
            val_list.pop(0)
            val_list.pop(-1)

            # get average value in list
            avg_val = reduce(lambda x, y: x + y, val_list) / len(val_list)
            return True, round(avg_val, 1)
        else:
            return True, random.randint(200, 300) / 10.0

    def run(self):
        """
        Get pH value and temperature and perform calibration.
        pH(correct) = pH(captured) + ZP + (T - 25) * 0.05916 * S / 100
        """
        ph_val = self.get_ph_val()
        tmp_valid, tmp_val = self.get_temperature()
        orp_val = self.get_orp_val()

        print {"pH": ph_val, "ORP": orp_val, "temp": tmp_val}

    def calibrate(self, buffer_val):
        try:
            if buffer_val not in [7, 4, 10]:
                return False
            else:
                buffer_val = float(buffer_val)
                val = self.adc.read_adc(self.ph_ads_pin_num, gain=self.ads_gain)
                voltage = val * 4.096 / self.ads_gain / 65536.0 * 2  # multiple 2 due to the sign
                if self.get_param_from_xml('pH_CAL_STEP') == '1':
                    self.set_param_to_xml("LAST_PH_VOLT", str(voltage))
                    self.set_param_to_xml('LAST_BUF', str(buffer_val))
                    self.set_param_to_xml('pH_CAL_STEP', '2')
                    print 'First step of calibration!'
                    return True, 'First Step'
                else:
                    old_volt = float(self.get_param_from_xml('LAST_PH_VOLT'))
                    old_buf = float(self.get_param_from_xml('LAST_BUF'))
                    if old_buf == buffer_val:
                        logging.error('Error, calibration cannot be done with the same buffer')
                        return False, 'Buffer should not be same...'
                    gradient = round((voltage - old_volt) / (buffer_val - old_buf), 3)
                    self.set_param_to_xml('pH_CAL_STEP', '1')
                    self.set_param_to_xml('pH_GRADIENT', str(gradient))
                    return True, 'Success'
        except Exception as e:
            return False, e


if __name__ == '__main__':

    print "Initializing Sensor..."
    a = RPiPool()

    while True:
        a.update_params()
        start_time = time.time()
        a.run()

        while time.time() - start_time < a.rate:
            time.sleep(0.2)
