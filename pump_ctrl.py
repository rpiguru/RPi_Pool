"""
Pump controller.
Actually it is PWM output controller.

Our pumps work when PWM wave length is greater than 500.

Example usage:

    sudo python pump_ctrl.py <pump number> <duration>

        ex: sudo python pump_ctrl.py 1 3

"""
import os
import threading
import time

import sys

try:
    is_rpi = True if os.uname()[4][:3] == 'arm' else False
except AttributeError:
    is_rpi = False

if is_rpi:
    import wiringpi


# The base frequency is 19.2kHz, so we can get 480Hz by dividing with 40
PWM_DIVIDER = 40


class PumpCtrl:

    _pump1 = 12
    _pump2 = 13

    def __init__(self, pump_pin_1=12, pump_pin_2=13):
        self._pump1 = pump_pin_1
        self._pump2 = pump_pin_2
        if is_rpi:
            self.initialize_pwm()

    def initialize_pwm(self):
        wiringpi.wiringPiSetupGpio()
        wiringpi.pinMode(self._pump1, 2)
        wiringpi.pinMode(self._pump2, 2)
        wiringpi.pwmSetClock(PWM_DIVIDER)
        # This function must be called after `pwmSetClock` function
        # I had to use my oscilloscope to find this, and it took about couple of hours to find this!
        wiringpi.pwmSetMode(wiringpi.PWM_MODE_MS)
        wiringpi.pwmWrite(self._pump1, 0)
        wiringpi.pwmWrite(self._pump2, 0)

    def _set_pwm(self, pump_num, cycle):
        if pump_num not in [1, 2]:
            raise ValueError('Pump number must be 1 or 2')
        if cycle < 0:
            print 'Warning, duty cycle of PWM output should be greater than 0, current: {}'.format(cycle)
            duty_cycle = 0
        elif cycle > 1023:
            print 'Warning, duty cycle of PWM output should be less than 1023, current: {}'.format(cycle)
            duty_cycle = 1023
        else:
            duty_cycle = cycle

        if pump_num == 1:
            print 'Driving pump 1, pin: {}, cycle: {}'.format(self._pump1, duty_cycle)
            wiringpi.pwmWrite(self._pump1, duty_cycle)
        else:
            print 'Driving pump 2, pin: {}, cycle: {}'.format(self._pump2, duty_cycle)
            wiringpi.pwmWrite(self._pump2, duty_cycle)

        return True

    def _perform_pump(self, pump_num=1, cycle=375, duration=5):
        self._set_pwm(pump_num=pump_num, cycle=cycle)
        time.sleep(duration)
        self._set_pwm(pump_num=pump_num, cycle=0)

    def stop_pump(self, pump_num=1):
        """
        Stop pump
        :param pump_num:
        :return:
        """
        self._set_pwm(pump_num=pump_num, cycle=0)

    def drive_pump(self, pump_num=1, cycle=375, duration=5):
        """
        Drive pump for given duration.
        :param pump_num: pump number
        :param cycle: speed, we tested and it says that 1 sec duration is equal with 1ml with cycle=375
        :param duration: duration in seconds
        :return:
        """
        threading.Thread(target=self._perform_pump,
                         kwargs={'pump_num': pump_num, 'cycle': cycle, 'duration': duration}).start()

if __name__ == '__main__':
    if is_rpi:
        if os.geteuid() != 0:
            exit("You need to have root privileges to run this script\nPlease try again with `sudo`\nExiting...")

    a = PumpCtrl()

    if len(sys.argv) == 1:
        # Drive pump 1 for 5 seconds
        a.drive_pump(pump_num=1, duration=5)

        # Drive pump 2 for 3 seconds
        a.drive_pump(pump_num=2, duration=3)
    else:
        a.drive_pump(pump_num=int(sys.argv[1]), duration=int(sys.argv[2]))

