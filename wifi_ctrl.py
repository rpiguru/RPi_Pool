import os
import subprocess
from base import Base
import wifi
import socket
import struct

try:
    is_rpi = True if os.uname()[4][:3] == 'arm' else False
except AttributeError:
    is_rpi = False

if is_rpi:
    import fcntl


class WiFiCtrl(Base):

    def __init__(self):
        Base.__init__(self)

    @staticmethod
    def get_ap_list():
        try:
            l_cell = wifi.Cell.all('wlan0')
        except wifi.exceptions.InterfaceError:
            if is_rpi:
                # When wlan0 is down, turn it on and scan again.
                subprocess.call(['/sbin/ifup wlan0'], shell=True)
                l_cell = wifi.Cell.all('wlan0')
            else:
                return []
        except WindowsError:
            return []

        ap_list = []
        for cell in l_cell:
            ap_list.append(cell.ssid)
        return ap_list

    def connect_to_ap(self, ssid, pwd):
        """
        Modify /etc/network/interface file and turn off/on wlan0 interface with ifdown/ifup shell command
        :param ssid: New SSID of ap
        :param pwd: password
        :return:
        """
        # ---- modify /etc/network/interface file ----
        f = open('/etc/network/interfaces', 'r+b')
        f_content = f.readlines()
        line_num = 0
        for i in range(len(f_content)):
            if 'iface wlan0 inet' in f_content[i]:
                line_num = i
                break
        f_content[line_num] = 'iface wlan0 inet dhcp\n'
        try:
            f_content[line_num + 1] = '    wpa-ssid ' + ssid + '\n'
        except IndexError:
            f_content.append('    wpa-ssid ' + ssid + '\n')

        try:
            f_content[line_num + 2] = '    wpa-psk ' + pwd + '\n'
        except IndexError:
            f_content.append('    wpa-psk ' + pwd + '\n')

        for i in range(len(f_content)):
            if 'wpa-conf /etc/wpa' in f_content[i] and '#' not in f_content[i]:
                f_content[i] = "#  " + f_content[i]

        f.seek(0)
        f.truncate()
        f.write(''.join(f_content))
        f.close()
        os.system('/usr/bin/sudo /sbin/ifdown wlan0')
        os.system('/usr/bin/sudo /sbin/ifup wlan0')

        ip = self.get_ip_address('wlan0')

        return ip

    @staticmethod
    def get_current_ap():
        """
        Get currently connected AP's SSID
        :return:
        """
        pipe = os.popen('iwgetid -r')
        data = pipe.read().strip()
        pipe.close()
        return data

    @staticmethod
    def get_ip_address(ifname):
        """
        Get assigned IP address of given interface
        :param ifname: interface name such as wlan0, eth0, etc
        :return:
        """
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            ip = socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s', ifname[:15]))[20:24])
            return ip
        except IOError:
            return None
