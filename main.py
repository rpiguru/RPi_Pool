import threading

from kivy.app import App

import configure

import datetime
import glob
import logging.config
import os
import time
from functools import partial
from kivy.clock import Clock, mainthread
from kivy.lang import Builder
from kivy.properties import StringProperty, ListProperty, ObjectProperty, NumericProperty
from kivy.uix.button import Button
from kivy.uix.dropdown import DropDown
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import NoTransition
from kivy.uix.screenmanager import SlideTransition

from base import Base
from flow_ctrl import FlowCtrl
from murano_ctrl import MuranoCtrl
from pump_ctrl import PumpCtrl
from rpi_pool import RPiPool
from wifi_ctrl import WiFiCtrl

try:
    is_rpi = True if os.uname()[4][:3] == 'arm' else False
except AttributeError:
    is_rpi = False

cur_dir = os.path.dirname(os.path.realpath(__file__)) + '/'

try:
    logging.config.fileConfig(cur_dir + "logging.conf")
except IOError:
    print 'Failed to load configuration of logging'

logger = logging.getLogger("pool")


class CautionPopup(Popup):
    pass


class ConfirmPopup(Popup):
    pass


class ModePopup(Popup):
    pass


pump_val = {
    'minus': {'SOFT': 10, 'HARD': 7, 'VERY HARD': 5},
    'plus': {'SOFT': 5, 'HARD': 7, 'VERY HARD': 10},
}
pump_num = {'minus': 1, 'plus': 2}


class MainApp(App, Base):

    _current_title = StringProperty()        # Store title of current screen
    _screen_names = ListProperty([])
    _screens = {}                            # Dict of all screens
    _hierarchy = ListProperty([])

    # Popup instances
    _caution_popup = ObjectProperty(None)
    _confirm_popup = ObjectProperty(None)
    _mode_popup = ObjectProperty(None)
    _confirm_param = ListProperty([])        # Parameter used in 'confirm popup'

    _wifi = ObjectProperty(None)                        # wifi instance
    _murano = ObjectProperty(None)
    _pool = ObjectProperty(None)
    _flow = ObjectProperty(None)
    _pump = ObjectProperty(None)
    _mode = StringProperty('AUTO')
    _vol = NumericProperty(0)           # Pool Volume in m3
    _pool_type = StringProperty('INDOOR')
    _filter_period = NumericProperty(2)
    _pump_type = StringProperty('STANDARD')
    _water_temp = StringProperty('<20')
    _water_hard = StringProperty('SOFT')
    s_time = NumericProperty(0)

    dropdown = DropDown()           # DropDown instance in 'WiFi Setting' screen
    dropdown_cal = DropDown()       # DropDown instance in 'Calibration Setting' screen

    def build(self):
        """
        base function of kivy app
        :return:
        """
        logger.debug(' ===== Starting Pool Controller =====')
        Base.__init__(self)

        self.load_screen()

        self._caution_popup = CautionPopup()
        self._confirm_popup = ConfirmPopup()
        self._mode_popup = ModePopup()

        self._confirm_param = [None] * 5

        self._wifi = WiFiCtrl()
        self._pool = RPiPool()
        self._flow = FlowCtrl()
        self._flow.start_sensoring()
        if is_rpi:
            self._pump = PumpCtrl()
            self._pump.stop_pump(1)
            self._pump.stop_pump(2)

        self._murano = MuranoCtrl(
            product_id=self.get_param_from_xml('PRODUCT_ID'),
            identifier=self.get_param_from_xml('IDENTIFIER'),
            device_cik=self.get_param_from_xml('DEVICE_CIK')
            )

        self.go_screen('menu', 'right')

        # We need to delay 10 sec for the activation with Murano...
        Clock.schedule_once(self.activate_murano, 10)

        if not self._pool.update_params():
            logger.error('Failed to initialize sensors.')
        # self.go_screen('menu', 'right')
        Clock.schedule_interval(self.show_clock, 1)

        threading.Thread(target=self.upload_sensor_data).start()
        threading.Thread(target=self.main_exec).start()

        # Clock.schedule_interval(self.main_exec, int(self.get_param_from_xml('READING_RATE')))

        self.initialize()

    def initialize(self):
        """
        Generic initializing function
        :return:
        """
        # Save starting time
        self.s_time = time.time()
        # Operating mode
        self._mode = self.get_param_from_xml('MODE')
        if self._mode == 'AUTO':
            self._screens['menu'].ids['chk_mode_auto'].active = True
        else:
            self._screens['menu'].ids['chk_mode_manual'].active = True

        # Pool Volume
        self._vol = int(self.get_param_from_xml('POOL_VOLUME'))
        self._screens['settings_parameter'].ids['sld_pool_vol'].value = self._vol

        # Pool type
        self._pool_type = self.get_param_from_xml('POOL_TYPE')
        if self._pool_type == 'INDOOR':
            self._screens['settings_parameter'].ids['chk_pool_indoor'].active = True
        else:
            self._screens['settings_parameter'].ids['chk_pool_outdoor'].active = True

        # Filter period
        self._filter_period = int(self.get_param_from_xml('FILTER_PERIOD'))
        self._screens['settings_parameter'].ids['sld_filter_period'].value = self._filter_period

        # Pump Type
        self._pump_type = self.get_param_from_xml('PUMP_TYPE')
        if self._pump_type == 'STANDARD':
            self._screens['settings_parameter'].ids['chk_pump_standard'].active = True
        else:
            self._screens['settings_parameter'].ids['chk_pump_variable'].active = True

        # Average Water Temperature
        self._water_temp = self.get_param_from_xml('AVG_WATER_TEMP')
        if self._water_temp == '20':
            self._screens['settings_parameter_water'].ids['chk_avg_water_temp_0'].active = True
        elif self._water_temp == '20-30':
            self._screens['settings_parameter_water'].ids['chk_avg_water_temp_1'].active = True
        else:
            self._screens['settings_parameter_water'].ids['chk_avg_water_temp_2'].active = True

        self._water_hard = self.get_param_from_xml('WATER_HARDNESS')
        if self._water_hard == 'SOFT':
            self._screens['settings_parameter_water'].ids['chk_water_hard_soft'].active = True
        elif self._water_hard == 'HARD':
            self._screens['settings_parameter_water'].ids['chk_water_hard_hard'].active = True
        else:
            self._screens['settings_parameter_water'].ids['chk_water_hard_very_hard'].active = True

        # Add dropdown buttons to calibration settings window
        buf_list = ['pH buf 4.0', 'pH buf 7.0', 'pH buf 10.0']
        self.dropdown_cal.clear_widgets()
        for buf_name in buf_list:
            btn = Button(text=buf_name, size_hint_y=None, height=35)
            btn.bind(on_release=lambda b: self.dropdown_cal.select(b.text))
            self.dropdown_cal.add_widget(btn)
        main_btn = self._screens['settings_calibration'].ids['btn_dropdown']
        main_btn.bind(on_release=self.dropdown_cal.open)
        self.dropdown_cal.bind(on_select=lambda instance, x: setattr(main_btn, 'text', x))
        main_btn.text = buf_list[0]

        self._mode_popup.dismiss()

    def activate_murano(self, *args):
        threading.Thread(target=self.tr_activate).start()

    def tr_activate(self, *args):
        status, msg = self._murano.activate()

        # If this is the 1st time of activation, store new cik value.
        if status:
            self.set_param_to_xml('DEVICE_CIK', msg)
            logger.info('Successfully saved new CIK value')
            return True
        elif status is None:
            logger.error('Failed to activate murano...')
            Clock.schedule_once(self.show_activate_error_popup)
        elif not status:        # Device is already activated.
            return True

    @mainthread
    def show_activate_error_popup(self, *args):
        Popup(content=Label(text='Failed to activate to server.\nPlease contact POOLTEAM to resolve this.',
                            font_size=17), size_hint=(.8, .5), title='Activation error').open()

    def main_exec(self, *args):
        """
        Main reading function
        :param args:
        :return:
        """
        _cnt = 0
        while True:
            if _cnt < 3:
                self.adjust_ph()
                _cnt += 1
            else:
                self.adjust_ph()
                _cnt = 0
                ph_val = self._pool.get_ph_val()
                if 6.5 < ph_val < 7.6:
                    orp_val = self._pool.get_orp_val() * 1000  # V => mV
                    if self._mode == 'AUTO':
                        if ph_val > 7.3:
                            orp_sp = 750
                        else:
                            orp_sp = 770
                    else:
                        orp_sp = self.get_param_from_xml('ORP_SETPOINT')
                    if orp_val > orp_sp:
                        orp_ml = (orp_val - orp_sp) / 100
                        logger.debug('dosage fluent chlorine liquid: {}'.format(orp_ml))
                        # TODO: How to?

            time.sleep(24 / self._filter_period * 4 * 3600)

    def upload_sensor_data(self, *args):
        """
        Read sensor data and upload to murano
        :param args:
        :return:
        """
        while True:
            ph_val = self._pool.get_ph_val()
            orp_val = self._pool.get_orp_val()
            temp_valid, temp_val = self._pool.get_temperature()
            flow_val = self._flow.get_flow() * 3600 / 1000.0 / 1000.0  # ml/sec to m3/Hr

            if temp_valid is None:
                logger.critical('Failed to read temperature, please check temp sensor')
                # Use ideal value...
                temp_val = 25
            # Upload sensor data to murano
            req_str = 'pH={}&ORP={}&Temperature={}&Flow={}'.format(ph_val, orp_val, temp_val, flow_val)
            logger.debug('Uploading sensor data: {}'.format(req_str))
            _status, _resp = self._murano.upload_to_murano(req_str)
            if not _status:
                logger.error('Failed to upload sensor data, response: {}'.format(_resp))

            Clock.schedule_once(partial(self.update_sensor_values, ph_val, orp_val, flow_val, temp_val))
            if is_rpi and not self.debug:
                time.sleep(60)
            else:
                time.sleep(5)

    @mainthread
    def update_sensor_values(self, ph_val, orp_val, flow_val, temp_val, *args):
        self._screens['menu'].ids['lb_ph'].text = '[b][color=111111]{}[/color][/b]'.format(ph_val)
        self._screens['menu'].ids['lb_orp'].text = '[b][color=111111]{}[/color][/b]'.format(orp_val)
        self._screens['menu'].ids['lb_flow'].text = '[b][color=111111]{}[/color][/b]'.format(round(flow_val, 2))
        self._screens['menu'].ids['lb_temp'].text = '[b][color=111111]{}[/color][/b]'.format(temp_val)

    def adjust_ph(self):
        """
        Read sensor values and adjust pH value
        :return:
        """
        ph_val = self._pool.get_ph_val()
        orp_val = self._pool.get_orp_val()
        flow_val = self._flow.get_flow() * 3600 / 1000.0 / 1000.0  # ml/sec to m3/Hr
        temp_valid, temp_val = self._pool.get_temperature()

        if temp_valid is None:
            logger.critical('Failed to read temperature, please check temp sensor')
            # Use ideal value...
            temp_val = 25

        print '-' * 40
        logger.debug('pH: {}, ORP: {}, TEMP: {}, FLOW: {}'.format(ph_val, orp_val, temp_val, flow_val))

        dose_type = None
        if self._mode == 'AUTO':
            if ph_val < 6.5 and 6.5 - ph_val > .1:
                dose_type = 'plus'
            elif ph_val > 7.6 and ph_val - 7.6 > .1:
                dose_type = 'minus'
        else:
            sp = float(self.get_param_from_xml('PH_SETPOINT'))
            if ph_val < sp and sp - ph_val > .1:
                dose_type = 'plus'
            elif ph_val > sp and ph_val - sp > .1:
                dose_type = 'minus'

        if dose_type is None:
            logger.debug('pH is very good.')
        else:
            if self._pump is not None:
                self._pump.drive_pump(pump_num=pump_num[dose_type],
                                      duration=pump_val[dose_type][self.get_param_from_xml('WATER_HARDNESS')])

    def confirm_yes(self):
        """
        callback function of "Yes" button in confirm popup
        :return:
        """
        self._confirm_popup.dismiss()
        if self._confirm_param:
            if self._confirm_param[0] == 'connect_to_ap':
                Clock.schedule_once(partial(self.connect_to_ap(self._confirm_param[1], self._confirm_param[2])), 1)

    def confirm_no(self):
        """
        callback function of "No" button in confirm popup
        :return:
        """
        self._confirm_popup.dismiss()
        if self._confirm_param[0] == 'set_relay_from_maintenance':
            pass

    def go_screen(self, dest_screen, direction):
        """
        Go to given screen
        :param dest_screen:     destination screen name
        :param direction:       "up", "down", "right", "left"
        :return:
        """
        logger.debug('Move to screen {}'.format(dest_screen))
        sm = self.root.ids.sm
        if dest_screen == 'settings_wifi':
            self.root.ids['lb_title'].text = 'WiFi SETTINGS'
            self.update_ap_list()
        elif dest_screen == 'settings':
            self.root.ids['lb_title'].text = 'SETTINGS'
        elif dest_screen == 'menu':
            self.root.ids['lb_title'].text = 'Clear Water'
        elif dest_screen == 'settings_calibration':
            self.root.ids['lb_title'].text = 'pH CALIBRATION'
        elif dest_screen == 'settings_parameter':
            self.root.ids['lb_title'].text = 'SETTINGS(POOL)'
        elif dest_screen == 'settings_test_dosing':
            self.root.ids['lb_title'].text = 'TEST DOSING'
        elif dest_screen == 'settings_parameter_water':
            self.root.ids['lb_title'].text = 'SETTINGS(WATER)'

        if 'settings_parameter' in self._current_title and 'settings_parameter' in dest_screen:
            sm.transition = NoTransition()
        else:
            sm.transition = SlideTransition()

        screen = self._screens[dest_screen]
        sm.switch_to(screen, direction=direction)
        self._current_title = screen.name

    def load_screen(self):
        """
        Load all screens from data/screens to Screen Manager
        :return:
        """
        available_screens = []

        full_path_screens = glob.glob(self.cur_dir + "screens/*.kv")

        for file_path in full_path_screens:
            file_name = os.path.basename(file_path)
            available_screens.append(file_name.split(".")[0])

        self._screen_names = available_screens
        for i in range(len(full_path_screens)):
            screen = Builder.load_file(full_path_screens[i])
            self._screens[available_screens[i]] = screen
        return True

    def update_ap_list(self):
        """
        Get surround WiFi AP list and update dropdown widget.
        :return:
        """
        ap_list = self._wifi.get_ap_list()
        logger.debug('Discovered AP list:')
        logger.debug(ap_list)
        # ap_list = ['AP 1', 'AP 2', 'AP 3', 'AP 4']
        self.dropdown.clear_widgets()

        for ap_name in ap_list:
            btn = Button(text=ap_name, size_hint_y=None, height=30)
            btn.bind(on_release=lambda btn: self.dropdown.select(btn.text))
            self.dropdown.add_widget(btn)

        main_btn = self._screens['settings_wifi'].ids['btn_dropdown']
        main_btn.bind(on_release=self.dropdown.open)

        self.dropdown.bind(on_select=lambda instance, x: setattr(main_btn, 'text', x))
        if len(ap_list) > 0:
            cur_ap = self._wifi.get_current_ap()
            if cur_ap != '' and cur_ap in ap_list:
                main_btn.text = cur_ap
                self._screens['settings_wifi'].ids['lb_cur_ap'].text = 'Current AP: {}'.format(cur_ap)
            else:
                main_btn.text = ''

    def btn_connect_to_ap(self):
        """
        This function is called when user presses 'Connect' button.
        :return:
        """
        pwd = self._screens['settings_wifi'].ids['txt_pwd'].text
        ssid = self._screens['settings_wifi'].ids['btn_dropdown'].text
        self._confirm_popup.ids['lb_content'].text = 'Connect to ' + ssid + "?\nIt will take a while"
        self._confirm_param[0] = 'connect_to_ap'
        self._confirm_param[1] = ssid
        self._confirm_param[2] = pwd
        self._confirm_popup.open()

    def connect_to_ap(self, ssid, pwd, *args):
        logger.debug('Connecting to `{}` AP'.format(ssid))
        new_ip = self._wifi.connect_to_ap(ssid=ssid, pwd=pwd)
        if new_ip is not None:
            self._screens['settings_wifi'].ids['txt_pwd'].text = ''
            self._caution_popup.ids['lb_content'].text = "New IP: " + new_ip
            self._caution_popup.open()
            self.go_screen('menu', 'right')
            return True
        else:
            self._caution_popup.ids['lb_content'].text = "Failed to connect to AP\nPlease check SSID & password"
            self._caution_popup.open()
            return False

    def update_settings(self):
        """
        Update settings to the config file...
        :return:
        """
        new_token = self._screens['settings'].ids['txt_token'].text
        if new_token.strip() == '':
            self._caution_popup.ids['lb_content'].text = 'Please input new token'
            self._caution_popup.open()
        else:
            self._confirm_popup.ids['lb_content'].text = 'Would you like to update settings?'
            self._confirm_param = ['update_token', ]
            self._confirm_popup.open()

    def perform_calibrate(self, sensor_type):
        """
        Perform calibration
        """
        if sensor_type == 'ph':
            buf_type = self._screens['settings_calibration'].ids['btn_dropdown'].text
            print 'Starting pH calibration, buffer: ', buf_type
            resp, state = self._pool.calibrate(float(buf_type.split(' ')[-1]))
            if resp:
                logger.info('Succeeded to calibrate: {}'.format(state))
            else:
                logger.error('Failed to calibrate: {}'.format(state))
            Popup(content=Label(text=str(state)), size_hint=(.8, .5), title='POOLTEAM').open()

        else:
            cur_orp_val = self._pool.get_orp_val()
            try:
                sample_val = int(self._screens['settings_calibration'].ids['txt_sample_orp'].text)
            except ValueError:
                Popup(content=Label(text='Invalid REDOX value'), size_hint=(.5, .5), title='POOLTEAM').open()
                return False

            logger.debug("New OPP offset: {}mV".format(sample_val - cur_orp_val))
            self.set_param_to_xml('ORP_OFFSET', sample_val - cur_orp_val)
            Popup(content=Label(text='Calibration done', title='POOLTEAM'), size_hint=(.5, .5)).open()
            return True

    def show_clock(self, *args):
        self.root.ids['lb_clock'].text = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S %Z')

    def on_change_checkbox(self, widget_type, widget, *lparam):
        """
        Callback function when the checkboxes of MODE are changed
        :param widget_type:
        :param widget:
        :param lparam: (new_value, screen_name)
        :return:
        """
        if widget.active:
            if widget_type == 'mode':
                logger.debug('Changing operation mode to {}'.format(lparam[0]))
                self._mode = lparam[0]
                self.set_param_to_xml('MODE', self._mode)
                if self._mode == 'MANUAL':
                    self._mode_popup.ids['txt_ph_sp'].text = self.get_param_from_xml('PH_SETPOINT')
                    self._mode_popup.ids['txt_orp_sp'].text = self.get_param_from_xml('ORP_SETPOINT')
                    self._mode_popup.open()
                _stat, _resp = self._murano.upload_to_murano('pH_mode={}'.format(self._mode))
                if not _stat:
                    logger.error('Failed to upload new pH mode({}), response: {}'.format(self._mode, _resp))
            elif widget_type == 'pool_type':
                logger.debug('Changing pool type to {}'.format(lparam[0]))
                self._pool_type = lparam[0]
                self.set_param_to_xml('POOL_TYPE', self._pool_type)
            elif widget_type == 'pump_type':
                logger.debug('Changing pump type to {}'.format(lparam[0]))
                self._pump_type = lparam[0]
                self.set_param_to_xml('PUMP_TYPE', self._pump_type)
            elif widget_type == 'water_temp':
                logger.debug('Changing AVD Water Temp to {}'.format(lparam[0]))
                self._water_temp = lparam[0]
                self.set_param_to_xml('AVG_WATER_TEMP', self._water_temp)
            elif widget_type == 'water_hard':
                logger.debug('Changing Water Hardness to {}'.format(lparam[0]))
                self._water_hard = lparam[0]
                self.set_param_to_xml('WATER_HARDNESS', self._water_hard)

    def on_change_slider(self, wid_type, wid):
        """
        Callback function when the slider of POOL_VOLUME changes
        :param wid_type: Widget type
        :param wid:
        :return:
        """
        if wid_type == 'pool_vol':
            self._vol = int(wid.value)
            self.set_param_to_xml('POOL_VOLUME', self._vol)
        elif wid_type == 'filter_period':
            self._filter_period = int(wid.value)
            self.set_param_to_xml('FILTER_PERIOD', self._filter_period)

    def set_manual_sp(self):
        """
        Set manual set point of pH and ORP
        :return:
        """
        try:
            new_ph_sp = float(self._mode_popup.ids['txt_ph_sp'].text)
            new_orp_sp = float(self._mode_popup.ids['txt_orp_sp'].text)
        except ValueError:
            Popup(content=Label(text='Invalid Type of value', title='POOLTEAM'), size_hint=(.5, .5)).open()
            return
        logger.debug('New pH Set Point: {}'.format(new_ph_sp))
        logger.debug('New ORP Set Point: {}'.format(new_orp_sp))
        req_str = 'pH_setpoint={}&ORP_setpoint={}'.format(new_ph_sp, new_orp_sp)
        _stat, _resp = self._murano.upload_to_murano(req_str)
        if not _stat:
            logger.error('Failed to upload new setpoints, response: {}'.format(_resp))
        self.set_param_to_xml('PH_SETPOINT', str(new_ph_sp))
        self.set_param_to_xml('ORP_SETPOINT', str(new_orp_sp))
        Popup(content=Label(text='Update Success'), size_hint=(.5, .5), title='POOLTEAM').open()
        self._mode_popup.dismiss()


if __name__ == '__main__':

    if is_rpi:
        if os.geteuid() != 0:
            exit("You need to have root privileges to run this script\nPlease try again with `sudo`\nExiting...")

    app = MainApp()
    app.run()
