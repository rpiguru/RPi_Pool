import os
from kivy.clock import Clock
from kivy.config import Config

Config.read(os.path.expanduser('~/.kivy/config.ini'))
Config.set('graphics', 'width', '480')
Config.set('graphics', 'height', '320')
Config.set('kivy', 'keyboard_mode', 'systemanddock')
Config.set('input', 'mtdev_%(name)s', 'probesysfs,provider=mtdev,param=rotation=270,param=invert_y=1')
Config.set('input', 'hid_%(name)s', 'probesysfs,provider=hidinput,param=rotation=270,param=invert_y=1')
Config.remove_option('input', '%(name)s')

Clock.max_iteration = 20
