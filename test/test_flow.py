"""
Flow reading script.
In fact, it counts how many times the sensor detects flow in a sec.

"""

import sys
import time
import RPi.GPIO as GPIO

FLOW_SENSOR = 22

GPIO.setmode(GPIO.BCM)
GPIO.setup(FLOW_SENSOR, GPIO.IN, pull_up_down=GPIO.PUD_UP)

count = 0


def count_pulse(channel):
    global count
    count += 1
    print count

GPIO.add_event_detect(FLOW_SENSOR, GPIO.FALLING, callback=count_pulse)

while True:
    try:
        if count > 0:
            print 'Count: ', count
        count = 0       # Initialize `count` every second
        time.sleep(1)
    except KeyboardInterrupt:
        print '\ncaught keyboard interrupt!, bye'
        GPIO.cleanup()
        sys.exit()
